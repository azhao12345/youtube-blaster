'use strict';
//requires
var pageMod = require('sdk/page-mod');
var queryString = require('sdk/querystring');
var self = require('sdk/self');

var jsStringEscape = require('./js-string-escape');

//require legcy apis
var {Cc, Ci, Cr} = require('chrome');


var TOPIC_MODIFY_REQUEST = 'http-on-modify-request';
var TOPIC_EXAMINE_RESPONSE = 'http-on-examine-response';

//turn a string into uri
function makeURI(aURL, aOriginCharset, aBaseURI) {
  var ioService = Cc["@mozilla.org/network/io-service;1"]
                  .getService(Ci.nsIIOService);
  return ioService.newURI(aURL, aOriginCharset, aBaseURI);
}

//get the observer service
var observerService = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);

//observer to turn fake page loads into real ones
var observer = 
{
    observe: function(aSubject, aTopic, aData)
    {
        if(TOPIC_MODIFY_REQUEST == aTopic)
        {
            var url;
            aSubject.QueryInterface(Ci.nsIHttpChannel);
            if(aSubject.URI.spec.indexOf('www.youtube.com/watch?v=') !== -1 && aSubject.URI.spec.indexOf('spf=navigate') !== -1)
            {
                var parts = aSubject.URI.spec.split('?');
                //remove that part you know
                var queryParams = queryString.parse(parts[1]);
                delete queryParams.spf;
                var newUrl = parts[0] + queryString.stringify(queryParams);
                console.log("redirectin:" + newUrl);
                aSubject.redirectTo(makeURI(newUrl));
            }
        }
    }
};
observerService.addObserver(observer, TOPIC_MODIFY_REQUEST, false);

//code for response stream listener
 function TracingListener() {
    this.originalListener = null;
    this.receivedData = [];
    this.dataCount = 0;
}

TracingListener.prototype =
{
    onDataAvailable: function(request, context, inputStream, offset, count) {
       // this.originalListener.onDataAvailable(request, context, inputStream, offset, count);
       //update amount of data
       this.dataCount += count;
        
        //get the datas
        var binaryInputStream = Cc['@mozilla.org/binaryinputstream;1'].createInstance(Ci['nsIBinaryInputStream']);
        binaryInputStream.setInputStream(inputStream);

        var data = binaryInputStream.readBytes(count);
        this.receivedData.push(data);
    },

    onStartRequest: function(request, context) {
        this.originalListener.onStartRequest(request, context);
    },

    onStopRequest: function(request, context, statusCode) {
        //mod the data we collected, then smash it into the final thing
        console.log(this.receivedData[0]);
        var testRet = '<script type="text/javascript">var rawString = "' + jsStringEscape(this.receivedData.join('')) + '";\n' + self.data.load('blastVideo.js') + '</script>';
        //var testRet = this.receivedData.join('');
        //var testRet = '     ' + testRet + 'woww';
        //console.log(testRet);
        //create streams
        var storageStream = Cc['@mozilla.org/storagestream;1'].createInstance(Ci['nsIStorageStream']);
        var binaryOutputStream = Cc['@mozilla.org/binaryoutputstream;1'].createInstance(Ci['nsIBinaryOutputStream'])

        //TODO choose a sane value here
        storageStream.init(8192, testRet.length, null);
        binaryOutputStream.setOutputStream(storageStream.getOutputStream(0));

        //write the data to the stream
        binaryOutputStream.writeBytes(testRet, testRet.length);
        this.originalListener.onDataAvailable(request, context, storageStream.newInputStream(0), 0, testRet.length);
        this.originalListener.onStopRequest(request, context, statusCode);
    },

    QueryInterface: function (aIID) {
        if (aIID.equals(Ci.nsIStreamListener) ||
            aIID.equals(Ci.nsISupports)) {
            return this;
        }
        throw Cr.NS_NOINTERFACE;
    }
}

//observer to intercept responses
var responseObserver = 
{
    observe: function(aSubject, aTopic, aData)
    {
        if(TOPIC_EXAMINE_RESPONSE == aTopic)
        {
            var url;
            aSubject.QueryInterface(Ci.nsIHttpChannel);
            if(aSubject.URI.spec.indexOf('www.youtube.com/watch?v=') !== -1)
            {
                var newListener = new TracingListener();
                aSubject.QueryInterface(Ci.nsITraceableChannel);
                newListener.originalListener = aSubject.setNewListener(newListener);
            } else if(aSubject.URI.spec.indexOf('ytimg.com') !== -1) {
                console.log('accesscontrolling');
                httpChannel.setResponseHeader(
                   'Access-Control-Allow-Origin',
                   'https://www.youtube.com',
                   false
                );
                httpChannel.setResponseHeader(
                   'Access-Control-Allow-Origin',
                   'https://www.youtube.com',
                   false
                );
                httpChannel.setResponseHeader(
                   'Access-Control-Allow-Origin',
                   'https://www.youtube.com',
                   false
                );
            }

        }
    }
}
observerService.addObserver(responseObserver, TOPIC_EXAMINE_RESPONSE, false);
/*
pageMod.PageMod(
{
    include: 'https://www.youtube.com/watch*',
    contentScriptFile: [self.data.url('blastVideo.js')],
    attachTo: ['top']
});
*/

//Content-Type header contains "json"
/*
pageMod.PageMod(
{
    include: '*.youtube.com',
    contentScriptFile: [self.data.url('blastRedirect.js')],
    attachTo: ['top']
});
*/
