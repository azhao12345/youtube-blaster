//vendor code
var decodeQueryString = function(queryString) {
  var key, keyValPair, keyValPairs, r, val, _i, _len;
  r = {};
  keyValPairs = queryString.split("&");
  for (_i = 0, _len = keyValPairs.length; _i < _len; _i++) {
    keyValPair = keyValPairs[_i];
    key = decodeURIComponent(keyValPair.split("=")[0]);
    val = decodeURIComponent(keyValPair.split("=")[1] || "");
    r[key] = val;
  }
  return r;
};
var decodeStreamMap = function(url_encoded_fmt_stream_map) {
  var quality, sources, stream, type, urlEncodedStream, _i, _len, _ref;
  sources = {};
  _ref = url_encoded_fmt_stream_map.split(",");
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    urlEncodedStream = _ref[_i];
    stream = decodeQueryString(urlEncodedStream);
    console.log(stream);
    type = stream.type.split(";")[0];
    quality = stream.quality.split(",")[0];
    stream.original_url = stream.url;

    var signature = stream.sig;
    if(signature === undefined && stream.url.indexOf('signature') == -1) {
        console.log('decrypting');
        var signature = decryptSignature(stream.s, player);
    }

    stream.url = "" + stream.url + "&signature=" + signature;
    //console.log(stream.url);
    sources["" + type + " " + quality] = stream;
  }
  return sources;
};

var playerScript = false;
var downloadPlayerjs = function(url) {
    if(!playerScript) {
        var x = new XMLHttpRequest();
        x.open('GET', url, false);
        x.send();
        playerScript = x.responseText;
    }
    return playerScript;
}

// assume all sigs for all qualities r the same
var cachedSignature = false;
var decryptSignature = function(s, playerUrl) {
    if(!cachedSignature) {
        var script = downloadPlayerjs(playerUrl);
        console.log(script.length);
        var re = /\.sig\|\|([a-zA-Z0-9$]+)\(/g
        try {
            var funcName = re.exec(script)[1];
        } catch (e) { 
            re = /["\']signature["\']\s*,\s*([a-zA-Z0-9$]+)\(/;
            funcName = re.exec(script)[1];
        }
        script = script.replace(',' + funcName + ',', ',');
        var result = eval('var ' + funcName + ' = 5;' + script + ";" + funcName + '("' + s + '");');
        console.log(result);
        cachedSignature = result;
    }
    return cachedSignature;
};

var player;

document.addEventListener('DOMContentLoaded', function(event)
{
//here we're gonna slurp that map outta the thing
console.log('blasting video');
var re = /url_encoded_fmt_stream_map":"(.*?)"/g;
var response = re.exec(rawString)[1];

try {
// let's look for the player
  var re2 = /(\/yts\/jsbin\/player-.*?\/base.js)/g
  player = re2.exec(rawString)[1];
  console.log(player);
} catch (e) {
  console.log('something went wrong grabbing the player, but we\'ll try to continue');
  player = '';
}

console.log('here we go');
//console.log(response);
var video = {};
video.sources = decodeStreamMap(response.replace(/\\u0026/g, '&'));

video.getSource = function(type, quality) {
  var exact, key, lowest, source, _ref;
  lowest = null;
  exact = null;
  _ref = this.sources;
  for (key in _ref) {
    source = _ref[key];
    if (source.type.match(type)) {
      if (source.quality.match(quality)) {
        exact = source;
      } else {
        lowest = source;
      }
    }
  }
  return exact || lowest;
};

console.log(video.sources);

function createHandler(qualityLevel)
{
    return function()
    {
        var vid = document.createElement('video');
        vid.src = video.sources[qualityLevel].url;
        vid.autoPlay = true;
        vid.controls = true;
        document.body.appendChild(vid);
        vid.play();
    };
}
for(var qualityLevel in video.sources)
{
    var elem = document.createElement('div');
    elem.textContent = qualityLevel;
    elem.onclick = createHandler(qualityLevel);
    document.body.appendChild(elem);
}

});
